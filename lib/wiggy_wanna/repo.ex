defmodule WiggyWanna.Repo do
  use Ecto.Repo,
    otp_app: :wiggy_wanna,
    adapter: Ecto.Adapters.Postgres
end
