defmodule WiggyWanna.Formaters.Advice do
  def format_advice({:ok, %{"slip" => %{"advice" => advice}}}) do
    {:ok, advice}
  end

  def format_advice(_body) do
    {:ok, "Hit the bottom"}
  end
end
