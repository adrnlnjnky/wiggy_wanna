defmodule WiggyWanna.Formaters.Router do
  alias WiggyWanna.Formaters.Advice
  alias WiggyWanna.Formaters.Jokes

  def handle_call(parent, response) do
    route_call(parent, response)
  end

  # defp route_call(%{type: "string"}, %{body: body})
  defp route_call(_, %{body: body})
       when is_bitstring(body) do
    {:ok, body}
  end

  defp route_call(%{redirect: "advice"}, %{status_code: 200, body: body}) do
    body
    |> Jason.decode()
    |> Advice.format_advice()
  end

  defp route_call(%{redirect: "jokes"}, %{status_code: 200, body: body}) do
    body
    |> Jason.decode()
    |> Jokes.format_joke()
  end

  defp route_call(%{type: "map"}, %{status_code: 200, body: body}) do
    {:ok, %{body: body}}
  end

  defp route_call(parent, body) do
    IO.inspect(body)
    IO.inspect(parent)
    {:ok, "Unrouted response"}
  end
end
