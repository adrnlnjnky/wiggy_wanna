defmodule WiggyWanna.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      WiggyWanna.Repo,
      # Start the Telemetry supervisor
      WiggyWannaWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: WiggyWanna.PubSub},
      # Start the Endpoint (http/https)
      WiggyWannaWeb.Endpoint
      # Start a worker by calling: WiggyWanna.Worker.start_link(arg)
      # {WiggyWanna.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: WiggyWanna.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    WiggyWannaWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
