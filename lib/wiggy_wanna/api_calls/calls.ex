defmodule WiggyWanna.ApiCalls.Calls do
  require Logger
  alias WiggyWanna.ApiCalls.Responses

  @doc """
  Uses the context to extract call type.
  args can be url string or a map

  TODO --> seperate get from post calls.
  """
  def operator(parent, args, context) do
    args
    |> maybe_make_map
    |> set_headers
    |> make_call(context)
    |> Responses.handle_response(parent, context)
  end

  # NOTE: What about lists?
  defp maybe_make_map(%{url: _, body: _, options: _} = args), do: args
  defp maybe_make_map(%{url: _, body: _} = args), do: Map.put(args, :options, [])
  defp maybe_make_map(%{url: _, options: _} = args), do: Map.put(args, :body, [])
  defp maybe_make_map(%{url: url}), do: %{url: url, body: %{}, options: []}
  defp maybe_make_map(args) when is_bitstring(args), do: %{url: args, body: %{}, options: []}

  defp maybe_make_map(args) do
    Logger.info("Hit bottom of ApiCalls.Calls.maybe_make_map")
    {:error, args}
  end

  defp set_headers(%{headers: headers} = args) when headers != nil, do: args

  defp set_headers(args) do
    Map.put(args, :headers, [{"Content-Type", "Application/json"}])
  end

  defp make_call(%{url: url, headers: headers, options: options}, %{
         parent_type: %{identifier: :query}
       }) do
    HTTPoison.get(url, headers, options)
  end

  defp make_call(%{url: url, body: body, headers: headers, options: options}, %{

         parent_type: %{identifier: :mutation}
       }) do
    HTTPoison.post(url, body, headers, options)
  end

  defp make_call(_url, _context) do
    Logger.warn("Hit the Bottom of the make_call function!!!")
    {:ok, %{message: "Sorry we may still be under construction"}}
  end
end

