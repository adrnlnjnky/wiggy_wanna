defmodule WiggyWanna.ApiCalls.ErrorHandler do
  require Logger

  def handle_errors(parent, response, %{definition: %{schema_node: %{type: type}}}) do
    Logger.info("Hit the Api Error handler")
    handle_error(parent, response, type)
  end

  defp handle_error(_, %{status_code: code, reason: reason}, :map) do
    {:ok, %{code: code, response: reason}}
  end

  defp handle_error(_, %{status_code: code}, :map) do
    {:ok, %{code: code, response: "No response given"}}
  end

  defp handle_error(_, %{status_code: code, reason: reason}, :string) do
    {:ok, "(#{code}) Reason: #{reason}"}
  end

  defp handle_error(_, %{status_code: code}, :string) do
    {:ok, "#{code}"}
  end

  defp handle_error(_, %{id: id, reason: reason}, _) do
    {:ok, "Error Handler: got id: #{id} and reason: #{reason}"}
  end

  defp handle_error(_parent, _response, _context) do
    Logger.warn("Hit the bottom of the handle_error function")
    {:ok, "Sorry unresolved error: Please try again later"}
  end

end
