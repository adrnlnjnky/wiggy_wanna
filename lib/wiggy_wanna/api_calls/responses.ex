defmodule WiggyWanna.ApiCalls.Responses do
  require Logger

  alias WiggyWanna.ApiCalls.ErrorHandler
  alias WiggyWanna.ApiCalls.Calls

    def handle_response({:ok, %{headers: headers, body: body}} = response, parent, context) do
      {_, type} = List.keyfind(headers, "Content-Type", 0)

      if Regex.match?(~r/json/, type) do
        Jason.decode(body)
      else
        body
      end
    route_response(parent, response, context)
  end

  def handle_response(response, parent, context) do
    Logger.info("Got a response with no Content-Type header")
    route_response(parent, response, context)
  end

  defp route_response(parent, {:error, response}, context) do
    ErrorHandler.handle_errors(parent, response, context)
  end

  defp route_response(_parent, {:ok,
    %{status_code: code, body: body}},
    %{definition: %{schema_node: %{type: :string}}})
       when code in 200..299 do
         {:ok, body}
  end

  # defp route_response(_parent, {:ok, %{status_code: code} = response}, _context)
  #      when code in 200..299, do: response

  defp route_response(_parent, {:ok, %{status_code: code} = response}, _context)
  when code in 200..299 do
  response
  end


  defp route_response(parent, {:ok, %{status_code: code} = response}, context)
       when code in 300..399 do
    time = DateTime.utc_now() |> DateTime.to_iso8601()
    Logger.warning("Got a redirect: #{code} at #{time}")
    redirect(parent, response, context)
  end

  defp route_response(parent, {:ok, %{status_code: code} = response}, context)
       when code in 400..599, do: ErrorHandler.handle_errors(parent, response, context)

  defp route_response(_parent, {:ok, %{status_code: code}}, _) do
    Logger.info({"Hit bottom of Responses.route_response. status_code", code})
    {:ok, "Responses.route_response got: status code #{code}"}
  end

  defp route_response(_parent, response, _context) do
    Logger.info({"Hit bottom of Responses.route_response. No status code provided", response})
    {:ok, "Sorry for the inconvenience, something appears to have gone wrong"}
  end

  # if new url is given just resend the call with the given url
  defp redirect(parent, %{method: "GET", url: url} = response, context) do
    Calls.operator(parent, Map.replace(response, :url, url), context)
  end

  defp redirect(_parent, %{status_code: code}, _) do
    {:ok, "handle the redirect got status code: #{code}"}
  end

end
