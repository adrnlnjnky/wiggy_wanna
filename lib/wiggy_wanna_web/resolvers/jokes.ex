defmodule WiggyWannaWeb.Resolvers.Jokes do
  alias WiggyWanna.ApiCalls.Calls

  @joke_api_base "https://v2.jokeapi.dev/joke/"
  # @dad_joke_base "https://dad-jokes.p.raparentapi.com/"

  def random_joke(parent, _, context) do
    Calls.operator(parent, @joke_api_base <> "any", context)
    |> decode_response
    |> handle_response
  end

  def joke_master(parent, %{category: _category, type: type}, context) do
    url = @joke_api_base <> type

    Calls.operator(parent, %{url: url, headers: []}, context)
  end

  def decode_response(%{body: body}), do: Jason.decode!(body)

  def handle_response(%{"flags" => %{"racist" => true, "sexist" => true}}),
    do: random_joke(%{}, "bad_joke", %{})

  def handle_response(
    %{"category" => cat, "setup" => setup, "delivery" => delivery, "safe" => safe}),
    do: {:ok, %{category: cat, setup: setup, delivery: delivery, safe: safe}}

  def handle_response(
    %{"category" => cat, "joke" => joke, "safe" => safe}),
    do: {:ok, %{category: cat, delivery: joke, safe: safe}}

    def handle_response(_body) do
      {:ok, "MISS handle_response MISS"}
    end

end

