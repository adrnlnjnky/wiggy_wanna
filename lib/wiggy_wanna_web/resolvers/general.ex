defmodule WiggyWannaWeb.Resolvers.General do
  alias WiggyWanna.ApiCalls.Calls

  @advice_slip_base "https://api.adviceslip.com/advice"
  def advice_slip(parent, %{search: search}, context),
    do: Calls.operator(parent, @advice_slip_base <> "/" <> search, context)

  def advice_slip(parent, _, context),
    do: Calls.operator(parent, @advice_slip_base, context)

  # def tv_maze(parent, %{show: show}, context),
    # do: Calls.operator(parent, "http://api.tvmaze.com/search/shows?q=#{show}", context)

  @pixelencouter_base "https://app.pixelencounter.com/api/basic/monsters/"
  def svg_monster(parent, _, context),
    do: Calls.operator(parent, @pixelencouter_base <> "random", context)

  # @other_thing "https://publicapi.example.com/oauth2/authorize?client_id=your_client_id&redirect_uri=your_url&response_type=code",
  # def other_thing(parent, _args, context),
  #   do: Calls.operator(parent, @other_thing, context)

end
