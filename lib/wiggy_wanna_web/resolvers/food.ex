defmodule WiggyWannaWeb.Resolvers.Food do
  alias WiggyWanna.ApiCalls.Calls

  @food_fact_base "http://world.openfoodfacts.org/api/v0/product"

  @food_recall_base "https://api.fda.gov/food/enforcement.json?limit="

  def food_fact(parent, %{name: tail}, _) do
    Calls.get_call(parent, @food_fact_base <> tail)
  end

  def food_fact(parent, _, _) do
    Calls.get_call(parent, @food_fact_base)
  end

  def food_recalls(parent, args, context) do
    headers = %{"Content-type" => "Appliction/json"}
    parent
    |> Map.put(:redirect, "food_recall")
    |> Map.put(:headers, headers)
    food_recalls_prep(parent, args, context)
  end

  defp food_recalls_prep(parent, %{count: limit}, _) do
    Calls.get_call(parent, @food_recall_base <> Integer.to_string(limit))
  end

  defp food_recalls_prep(parent, _, _context) do
    Calls.get_call(parent, @food_recall_base <> "10")
  end
end
