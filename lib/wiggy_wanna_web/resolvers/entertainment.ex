defmodule WiggyWannaWeb.Resolvers.Entertainment do  
  
  
  alias WiggyWanna.ApiCalls.Calls

  def tv_maze(parent, %{show: show}, context) do
    Calls.operator(parent, "http://api.tvmaze.com/search/shows?q=#{show}", context)
                                                                 |> handle_response
  end

  def tv_maze(_parent, args, _context) do
    IO.inspect(args)
    {:ok, "Missed the landing"}
    # Calls.operator(parent, "http://api.tvmaze.com/search/shows?q=#{show}", context)
  end

  def handle_response({:ok, response}) do
    thing = 
    response
    |> Jason.decode!

    # IO.inspect(Map.keys(thing))
    IO.inspect(thing)
    for stuff <- thing do
      IO.inspect("another one")
    end

    {:ok, "hit the handle_response"}
  end

  def handle_response(response) do
    IO.inspect(response)
    {:ok, "be patient while we fix our stuff"}
  end

end
