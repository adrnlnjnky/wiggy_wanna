defmodule WiggyWannaWeb.Resolvers.Users do
  alias WiggyWanna.ApiCalls.Calls

  @random_user "https://randomuser.me/api"

  def random_user(parent, _, _) do
    Calls.get_call(parent, @random_user)
    |> handle_response
  end

  # defp handle_response({:error, %{status_code: code}}) do
  #   {:ok, "Error code: #{code}"}
  # end

  defp handle_response({:ok, %{status_code: 200, body: body}}) do
    IO.inspect(body)
    {:ok, %{last_name: "Thorton"}}
  end

  defp handle_response({:error, _error}) do
    {:ok, "unhandled error"}
  end

  # def (_root, _args, _context) do
  
  # end



end
