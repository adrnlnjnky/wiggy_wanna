defmodule WiggyWannaWeb.Resolvers.Numbers do
  @numbers_base_api "http://numbersapi.com/"

  alias WiggyWanna.ApiCalls.Calls

  def number_fact(parent, %{number: number}, context) do
    Calls.operator(parent, @numbers_base_api <> Integer.to_string(number), context)
  end

  def number_fact(parent, _, context) do
    Calls.operator(parent, @numbers_base_api <> "random", context)
  end

end
