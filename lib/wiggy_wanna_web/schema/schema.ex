defmodule WiggyWannaWeb.Schema.Schema do
  use Absinthe.Schema

  alias WiggyWannaWeb.Resolvers
  import WiggyWannaWeb.Schema.Objects.Jokes

  import_types WiggyWannaWeb.Schema.Jokes
  import_types WiggyWannaWeb.Schema.Entertainment

  query do

      @desc "Facts about tv shows"
      field :tv_maze, list_of :tv_show do
        arg :show, :string
      resolve &Resolvers.Entertainment.tv_maze/3
    end

    # @desc "Food Recalls"
    # field :open_fda_food_recalls, :open_fda_food_recall do
    #   arg(:count, :integer)
    #   resolve(&Resolvers.Food.food_recalls/3)
    # end

    @desc "Get a fact about a food"
    field :food_fact, :string do
      arg(:name, :string)
      resolve(&Resolvers.Food.food_fact/3)
    end

    @desc "Advice Slip -- get a bit of advice"
    field :advice, :string do
      arg(:search, :string)
      resolve(&Resolvers.General.advice_slip/3)
    end

    field :number_fact, :string do
      @desc "Get a fact about a number. (Blank for random number)"
      self()
      arg(:number, :integer)
      resolve(&Resolvers.Numbers.number_fact/3)
    end

    @desc "Get a random joke"
    field :random_joke, :joke do
      resolve(&Resolvers.Jokes.random_joke/3)
    end

    # @desc "Get a random user"
    # field :random_user, :user do
    #   resolve(&Resolvers.Users.random_user/3)
    # end

  end

  #   field :disclaimer, :string
  # object :open_fda_food_recall do
  #   field :last_updated, :date
  #   field :country, :string
  #   field :states, :string
  #   field :city, :string
  #   field :reason_for_recall, :string
  #   field :product_quantity, :string
  #   field :code_info, :string
  #   field :distribution_pattern, :string
  #   field :status, :string
  # end

end
