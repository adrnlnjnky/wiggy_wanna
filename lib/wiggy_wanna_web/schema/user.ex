defmodule WiggyWannaWeb.Schema.User do
  use Absinthe.Schema.Notation

  import_types(WiggyWannaWeb.Schema.Helpers)

  object :street do
    field :name, :string
    field :number, :integer
  end

  object :lat_lon do
    field :latitude, :string
    field :longitude, :string
  end

  object :timezone do
    field :description, :string
    field :offset, :string
  end

  object :location do
    field :city, :string
    field :country, :string
    field :postcode, :integer
    field :state, :string
    field :coordinates, :lat_lon
    field :street, :street
    field :timezone, :timezone
  end

  object :id_info do
    field :name, :string
    field :value, :string
  end

  object :login_info do
    field :md5, :string
    field :password, :string
    field :salt, :string
    field :shal, :string
    field :sha256, :string
    field :username, :string
    field :uuid, :string
  end

  object :name do
    field :first, :string
    field :last, :string
    field :title, :string
  end

  object :registered do
    field :age, :integer
    field :date, :string
  end

  object :pictures do
    field :large, :string
    field :medium, :string
    field :thumbnail, :string
    field :registered, :registered
  end

  object :dob do
    field :age, :integer
    field :date, :string
  end

  object :user do
    field :cell, :string
    field :dob, :dob
    field :email, :string
    field :gender, :string
    field :id, :id_info
    field :location, :location
    field :login, :login_info
    field :name, :name
    field :nat, :string
    field :phone, :string
    field :picture, :pictures
  end
end
