defmodule WiggyWannaWeb.Schema.Helpers do
  use Absinthe.Schema.Notation

  object :err do
    field :code, :integer
    field :response, :string
  end
end
