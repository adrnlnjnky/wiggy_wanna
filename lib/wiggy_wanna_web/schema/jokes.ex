defmodule WiggyWannaWeb.Schema.Jokes do
  use Absinthe.Schema.Notation

  import_types WiggyWannaWeb.Schema.Helpers

  object :joke do
    field :category, :string
    field :delivery, :string
    field :flags, :flags
    field :id, :id
    field :lang, :string
    field :safe, :boolean
    field :setup, :string
    field :err, :err
  end

  object :flags do
    field :explicit, :boolean
    field :nsfw, :boolean
    field :political, :boolean
    field :racist, :boolean
    field :sexist, :boolean
  end

end
