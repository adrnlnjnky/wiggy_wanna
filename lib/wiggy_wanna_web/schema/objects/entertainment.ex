defmodule WiggyWannaWeb.Schema.Objects.Entertainment do  
  use Absinthe.Schema.Notation

  import_types WiggyWannaWeb.Schema.Helpers

  object :tv_show do
    field :name, :string
    field :score, :string
    field :err, :err
  end

end
